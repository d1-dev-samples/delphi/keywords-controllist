unit SortDirKeywordsRepo;

interface
uses
  Vcl.Graphics,
  System.Classes,
  KeywordsRepo;

type
  TSortDirection = (sdAscending, sdDescending);

  TSortDirectionKeywordsRepo = class(TComponent)
  private
    FRepo: TKeywordsRepo;
    FListsDir: TSortDirection;
    FKeywordsDir: TSortDirection;
    function TransformIndex(AIndex, ACount: integer; ADir: TSortDirection): integer;
  public
    constructor Create(AOwner: TComponent);

    function AddKeyword(AListIndex: integer; const AKeyword: string): integer;
    function RemoveKeyword(AListIndex: integer; AIndex: integer): boolean;
    function GetKeyword(AListIndex: integer; AIndex: integer): string;
    function GetKeywordCount(AListIndex: integer): integer;

    function AddList(const AListName: string; AColor: TColor): integer;
    function RemoveList(AListIndex: integer): boolean;
    function GetList(AListIndex: integer): TKeywordList;
    function GetListCount: integer;

    property KeywordsRepo: TKeywordsRepo read FRepo;
    property ListsDir: TSortDirection read FListsDir write FListsDir;
    property KeywordsDir: TSortDirection read FKeywordsDir write FKeywordsDir;
  end;


implementation

{ TSortDirectionKeywordsRepo }

constructor TSortDirectionKeywordsRepo.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FRepo := TKeywordsRepo.Create(AOwner);
end;


function TSortDirectionKeywordsRepo.TransformIndex(AIndex, ACount: integer; ADir: TSortDirection): integer;
begin
  if (ADir = sdDescending) and (AIndex >= 0) and (AIndex < ACount) then
    result := ACount - AIndex - 1
  else
    result := AIndex;
end;


function TSortDirectionKeywordsRepo.AddKeyword(AListIndex: integer; const AKeyword: string): integer;
begin
  AListIndex := TransformIndex(AListIndex, FRepo.GetCount, FListsDir);

  var klist := FRepo.Get(AListIndex);
  if klist = nil then
    exit(-1);

  result := klist.Add(AKeyword);
  result := transformIndex(result, klist.GetCount, FKeywordsDir);
end;


function TSortDirectionKeywordsRepo.AddList(const AListName: string; AColor: TColor): integer;
begin
  result := FRepo.Add(AListName, AColor);
  result := transformIndex(result, FRepo.GetCount, FListsDir);
end;


function TSortDirectionKeywordsRepo.GetKeyword(AListIndex, AIndex: integer): string;
begin
  AListIndex := TransformIndex(AListIndex, FRepo.GetCount, FListsDir);

  var klist := FRepo.Get(AListIndex);
  if klist = nil then
    exit('');

  AIndex := TransformIndex(AIndex, klist.GetCount, FKeywordsDir);
  result := klist.Get(AIndex);
end;


function TSortDirectionKeywordsRepo.GetKeywordCount(AListIndex: integer): integer;
begin
  AListIndex := TransformIndex(AListIndex, FRepo.GetCount, FListsDir);

  var klist := FRepo.Get(AListIndex);
  if klist = nil then
    exit(0);

  result := klist.GetCount;
end;


function TSortDirectionKeywordsRepo.GetList(AListIndex: integer): TKeywordList;
begin
  AListIndex := TransformIndex(AListIndex, FRepo.GetCount, FListsDir);

  result := FRepo.Get(AListIndex);
end;


function TSortDirectionKeywordsRepo.GetListCount: integer;
begin
  result := FRepo.GetCount;
end;


function TSortDirectionKeywordsRepo.RemoveKeyword(AListIndex, AIndex: integer): boolean;
begin
  AListIndex := TransformIndex(AListIndex, FRepo.GetCount, FListsDir);

  var klist := FRepo.Get(AListIndex);
  if klist = nil then
    exit(false);

  AIndex := TransformIndex(AIndex, klist.GetCount, FKeywordsDir);
  result := klist.Remove(AIndex);
end;


function TSortDirectionKeywordsRepo.RemoveList(AListIndex: integer): boolean;
begin
  AListIndex := TransformIndex(AListIndex, FRepo.GetCount, FListsDir);

  result := FRepo.Remove(AListIndex);
end;


end.
