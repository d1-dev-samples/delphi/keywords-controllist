unit main;

interface

uses
  SortDirKeywordsRepo,
  KeywordsRepo,
  KeywordsSaver,
  ControlListCheckbox,

  System.Math,
  Generics.Collections,
  System.Classes,
  Vcl.Graphics,
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.ExtCtrls,
  Vcl.ComCtrls,
  System.ImageList,
  Vcl.ImgList,
  Vcl.StdCtrls,
  Data.Bind.EngExt,
  Vcl.Bind.DBEngExt,
  System.Rtti,
  System.Bindings.Outputs,
  Vcl.Bind.Editors,
  Data.Bind.Components,
  Vcl.ToolWin,
  Vcl.Buttons,
  Vcl.Imaging.pngimage,
  Vcl.ControlList;

type
  TForm1 = class(TForm)
    pnlMasOuter: TPanel;
    Splitter1: TSplitter;
    pnlDetOuter: TPanel;
    pnlMaster: TPanel;
    pnlDetails: TPanel;
    Panel5: TPanel;
    pnlFooter: TPanel;
    pnlMasToolbar: TPanel;
    ImageList: TImageList;
    pnlDetToolbar: TPanel;
    btDefault: TButton;
    btClose: TButton;
    cbMasAll: TCheckBox;
    lbMasHeader: TLabel;
    imMasSort: TImage;
    pnlImprtExport: TPanel;
    imMasImport: TImage;
    imMasExport: TImage;
    lbDetHeader: TLabel;
    imDetSort: TImage;
    listMaster: TControlList;
    shMasFrame: TShape;
    shDetFrame: TShape;
    listDetails: TControlList;
    imMasAdd: TImage;
    lbMasText: TLabel;
    imMasList: TImage;
    imMasDelete: TImage;
    shMasColor: TShape;
    lbMasAdd: TLabel;
    edMas: TEdit;
    lbDetText: TLabel;
    lbDetAdd: TLabel;
    imDetDelete: TImage;
    edDet: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btDefaultClick(Sender: TObject);
    procedure btCloseClick(Sender: TObject);
    procedure cbMasAllMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure imMasImportClick(Sender: TObject);
    procedure imMasExportClick(Sender: TObject);
    procedure imMasSortClick(Sender: TObject);
    procedure imDetSortClick(Sender: TObject);
    procedure listMasterBeforeDrawItem(AIndex: Integer; ACanvas: TCanvas;
      ARect: TRect; AState: TOwnerDrawState);
    procedure listMasterClick(Sender: TObject);
    procedure listMasterItemClick(Sender: TObject);
    procedure listMasterMouseActivate(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y, HitTest: Integer;
      var MouseActivate: TMouseActivate);
    procedure listMasterMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure listMasterShowControl(const AIndex: Integer; AControl: TControl;
      var AVisible: Boolean);
    procedure EditMouseActivate(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y, HitTest: Integer;
      var MouseActivate: TMouseActivate);
    procedure EditKeyPress(Sender: TObject; var Key: Char);
    procedure EditExit(Sender: TObject);
    procedure edMasFinishEdit(Sender: TObject);
    procedure pnlMasOuterResize(Sender: TObject);
    procedure listDetailsMouseActivate(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y, HitTest: Integer;
      var MouseActivate: TMouseActivate);
    procedure listDetailsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure listDetailsShowControl(const AIndex: Integer; AControl: TControl;
      var AVisible: Boolean);
    procedure listDetailsBeforeDrawItem(AIndex: Integer; ACanvas: TCanvas;
      ARect: TRect; AState: TOwnerDrawState);
    procedure UpdateSelectionColor(Sender: TObject);
    procedure edDetClick(Sender: TObject);
    procedure pnlDetOuterResize(Sender: TObject);
    procedure listDetailsItemClick(Sender: TObject);
  private
    cbMasCheck: TControlListCheckbox;
    FRepo: TSortDirectionKeywordsRepo;
    FSaver: TKeywordsSaver;

    FMasMouse: TPoint;
    FDetMouse: TPoint;

    procedure AddMasterItem(Text: string);
    procedure AddDetailsItem(Text: string);
    procedure ChangeListColor(KeywordList: TKeywordList);
    procedure UpdateMasterFromModel;
    procedure UpdateDetailsFromModel;
    procedure UpdateCheckboxMasAll;
    procedure UpdateExportButton;
    function ChangeDir(AControlList: TControlList; Image: TImage; Dir: TSortDirection): TSortDirection;
    procedure UpdateMasterControls;
    function IsControlHot(AControl: TControl; AList: TControlList; AMouse: TPoint; LastRow: boolean): boolean;
    function GetHotOrDefaultCursor(IsHot: boolean): TCursor;
    procedure FinishEdit(Sender: TObject; Success: boolean);
    procedure StartEdit(AEdit: TEdit; AList: TControlList; ALeft: integer; ARight: integer);
    procedure UpdateDetailsControls;
    procedure DeleteMasterItem;
    procedure DeleteDetailsItem;
    procedure DoAfterItemSelectedMaster;
    procedure DoAfterItemSelectedDetails;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

const
  FILENAME_SAVE = 'keywords.json';

  IMG_PLUS = 13;
  IMG_SORT_ASC = 15;
  IMG_SORT_DESC = 14;
  IMG_IMPORT = 9;
  IMG_EXPORT = 7;
  IMG_EXPORT_DIS = 6;
  IMG_DELETE = 2;
  IMG_LIST = 10;


procedure TForm1.FormCreate(Sender: TObject);
begin
  ReportMemoryLeaksOnShutdown := True;

  lbMasText.Transparent := true;
  lbMasAdd.Transparent := true;
  lbDetText.Transparent := true;
  lbDetAdd.Transparent := true;

  edMas.Parent := listMaster;
  edDet.Parent := listDetails;

  cbMasCheck := TControlListCheckbox.Create(listMaster);
  cbMasCheck.Left := 11;
  cbMasCheck.top := 9;
  listMaster.AddControlToItem(cbMasCheck);

  FRepo := TSortDirectionKeywordsRepo.Create(self);
  FSaver := TKeywordsSaver.Create(self, FILENAME_SAVE);
  FSaver.load(FRepo.KeywordsRepo);

  UpdateMasterFromModel;
end;


procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FSaver.save(FRepo.KeywordsRepo);
end;


// adds new item (a list of keywords) in master
procedure TForm1.AddMasterItem(Text: string);
begin
  listMaster.ItemIndex := FRepo.AddList(text, random($1000000));
  if listMaster.ItemIndex < 0 then
    ShowMessage('������ � ����� ������ ��� ����������!')
  else
    listMaster.ItemCount := listMaster.ItemCount + 1;

  UpdateDetailsFromModel;
  listMaster.SetFocus;
end;


// adds new item (a keyword) in details
procedure TForm1.AddDetailsItem(Text: string);
begin
  listDetails.ItemIndex := FRepo.AddKeyword(listMaster.ItemIndex, text);
  if listDetails.ItemIndex < 0 then
    ShowMessage('� ������� ������ ����� �������� ����� ��� ����������!')
  else
    listDetails.ItemCount := listDetails.ItemCount + 1;
  listDetails.SetFocus;
end;


// delete an item from master
procedure TForm1.DeleteMasterItem;
begin
  if FRepo.RemoveList(listMaster.HotItemIndex) then
  begin
    listMaster.ItemCount := listMaster.ItemCount - 1;
    listMaster.ItemIndex := -1;
    DoAfterItemSelectedMaster;
  end;
end;


// delete an item from details
procedure TForm1.DeleteDetailsItem;
begin
  if FRepo.RemoveKeyword(listMaster.ItemIndex, listDetails.HotItemIndex) then
  begin
    listDetails.ItemCount := listDetails.ItemCount - 1;
    listDetails.ItemIndex := -1;
    DoAfterItemSelectedDetails;
  end;
end;


// opens the color dialog and changes the color in the selected list of keyword
procedure TForm1.ChangeListColor(KeywordList: TKeywordList);
begin
  with TColorDialog.Create(nil) do
    try
      Color := KeywordList.Color;
//      options := [cdFullOpen, cdSolidColor]; // let it be simple
      if Execute then
      begin
        KeywordList.Color := Color;
        listMaster.Invalidate;
      end;
    finally
      Free;
    end;
end;


// updates a bunch of stuff after changing item selection in master
procedure TForm1.DoAfterItemSelectedMaster;
begin
  UpdateSelectionColor(listMaster);
  UpdateDetailsFromModel;
  UpdateMasterControls;
  EditExit(edMas);
  EditExit(edDet);
  listMaster.Invalidate;
  UpdateExportButton;
end;


// updates a bunch of stuff after changing item selection in details
procedure TForm1.DoAfterItemSelectedDetails;
begin
  UpdateSelectionColor(listDetails);
  UpdateDetailsControls;
  EditExit(edMas);
  EditExit(edDet);
  listDetails.Invalidate;
end;


// stops editing if resizing master
procedure TForm1.pnlMasOuterResize(Sender: TObject);
begin
  EditExit(edMas);
end;


// stops editing if resizing details
procedure TForm1.pnlDetOuterResize(Sender: TObject);
begin
  EditExit(edDet);
end;


// updates master data from model
procedure TForm1.UpdateMasterFromModel;
begin
  listMaster.ItemCount := FRepo.GetListCount + 1;
  listMaster.ItemIndex := -1;
  DoAfterItemSelectedMaster;
  listMaster.Invalidate;
end;


// updates details data from model
procedure TForm1.UpdateDetailsFromModel;
begin
  if (listMaster.ItemIndex < 0) or (listMaster.ItemIndex >= listMaster.ItemCount - 1) then
    listDetails.ItemCount := 0
  else
    listDetails.ItemCount := FRepo.GetKeywordCount(listMaster.ItemIndex) + 1;

  listDetails.ItemIndex := -1;
  DoAfterItemSelectedDetails;
  listDetails.Invalidate;
end;


// updates master's common checkbox
procedure TForm1.UpdateCheckboxMasAll;
var
  FAll, FNone, FValue: boolean;
begin
  FAll := true;
  FNone := true;
  for var i := 0 to FRepo.GetListCount-1 do
  begin
    FAll := FAll and FRepo.GetList(i).Checked;
    FNone := FNone and (not FRepo.GetList(i).Checked);
  end;

  if FAll then
    cbMasAll.Checked := true
  else if FNone then
    cbMasAll.Checked := false
  else
    cbMasAll.State := TCheckBoxState.cbGrayed;
end;


// checks or unchecks all items in master
procedure TForm1.cbMasAllMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  for var i := 0 to FRepo.GetListCount-1 do
    FRepo.GetList(i).Checked := cbMasAll.Checked;
  listMaster.Invalidate;
end;


// updates export button state
procedure TForm1.UpdateExportButton;
begin
  imMasExport.Picture.Bitmap:= nil;
  if (listMaster.ItemCount > 1) and (listMaster.ItemIndex in [0 .. listMaster.ItemCount-2]) then
  begin
    ImageList.GetBitmap(IMG_EXPORT, imMasExport.Picture.Bitmap);
    imMasExport.Cursor := crHandPoint;
  end
  else
  begin
    ImageList.GetBitmap(IMG_EXPORT_DIS, imMasExport.Picture.Bitmap);
    imMasExport.Cursor := crDefault;
  end;
end;


// chages sort direction in master
procedure TForm1.imMasSortClick(Sender: TObject);
begin
  FRepo.ListsDir := ChangeDir(listMaster, imMasSort, FRepo.ListsDir);
end;


// chages sort direction in details
procedure TForm1.imDetSortClick(Sender: TObject);
begin
  FRepo.KeywordsDir := ChangeDir(listDetails, imDetSort, FRepo.KeywordsDir);
end;


// change direction helper
function TForm1.ChangeDir(AControlList: TControlList; Image: TImage; Dir: TSortDirection): TSortDirection;
begin
  if (AControlList.ItemIndex >= 0) and (AControlList.ItemIndex < AControlList.ItemCount - 1) then
    AControlList.ItemIndex := AControlList.ItemCount - AControlList.ItemIndex - 2;

  Image.Picture.Bitmap:= nil;
  if Dir = sdAscending then
  begin
    result := sdDescending;
    ImageList.GetBitmap(IMG_SORT_DESC, Image.Picture.Bitmap);
  end
  else
  begin
    result := sdAscending;
    ImageList.GetBitmap(IMG_SORT_ASC, Image.Picture.Bitmap);
  end;

  AControlList.Invalidate;
end;


// draw master
procedure TForm1.listMasterBeforeDrawItem(AIndex: Integer; ACanvas: TCanvas;
  ARect: TRect; AState: TOwnerDrawState);
begin
  var klist := FRepo.GetList(AIndex);
  if klist = nil then
    exit;

  lbMasText.Caption := klist.Name;
  shMasColor.Brush.Color := klist.Color;
  cbMasCheck.Checked := klist.Checked;

  var hot := AIndex = listMaster.HotItemIndex;
  cbMasCheck.Hot := hot;
end;


// draw details
procedure TForm1.listDetailsBeforeDrawItem(AIndex: Integer; ACanvas: TCanvas;
  ARect: TRect; AState: TOwnerDrawState);
begin
  lbDetText.Caption := FRepo.GetKeyword(listMaster.ItemIndex, AIndex);
end;


// show controls in master
procedure TForm1.listMasterShowControl(const AIndex: Integer;
  AControl: TControl; var AVisible: Boolean);
begin
  var last := AIndex = listMaster.ItemCount-1;
  var hot := AIndex = listMaster.HotItemIndex;

  imMasAdd.Visible := last and (not edMas.Visible);
  lbMasAdd.Visible := imMasAdd.Visible;

  imMasList.Visible := not last;
  lbMasText.Visible := not last;
  imMasDelete.Visible := (not last) and hot;
  shMasColor.Visible := not last;
  cbMasCheck.Visible := not last;
end;


// show controls in details
procedure TForm1.listDetailsShowControl(const AIndex: Integer;
  AControl: TControl; var AVisible: Boolean);
begin
  var last := AIndex = listDetails.ItemCount-1;
  var hot := AIndex = listDetails.HotItemIndex;

  lbDetAdd.Visible := last and (not edDet.Visible);
  lbDetText.Visible := not last;
  imDetDelete.Visible := (not last) and hot;
end;


// on selection changed in master
procedure TForm1.listMasterItemClick(Sender: TObject);
begin
  DoAfterItemSelectedMaster;
end;


// on selection changed in details
procedure TForm1.listDetailsItemClick(Sender: TObject);
begin
  DoAfterItemSelectedDetails;
end;


// updates selection bar color and makes it white for the last row
procedure TForm1.UpdateSelectionColor(Sender: TObject);
begin
  with Sender as TControlList do
  begin
    ItemSelectionOptions.FocusedColor := ifthen(ItemIndex = ItemCount-1, clWindow, clHighlight);
    ItemSelectionOptions.SelectedColor := ItemSelectionOptions.FocusedColor;
  end;
end;


// click on master (without consuming mouse event)
procedure TForm1.listMasterClick(Sender: TObject);
begin
  var klist := FRepo.GetList(listMaster.HotItemIndex);
  if klist = nil then
    exit;

  // click on checkbox in master
  if IsControlHot(cbMasCheck, listMaster, FMasMouse, false) then
  begin
    klist.Checked := not klist.Checked;
    cbMasCheck.Invalidate;
    UpdateCheckboxMasAll;
  end;

  // click on color in master
  if IsControlHot(shMasColor, listMaster, FMasMouse, false) then
  begin
    ChangeListColor(klist);
  end;
end;


// click on master (with consuming mouse event)
procedure TForm1.listMasterMouseActivate(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y, HitTest: Integer;
  var MouseActivate: TMouseActivate);
begin
  // delete from master
  if IsControlHot(imMasDelete, listMaster, FMasMouse, false) then
  begin
    DeleteMasterItem;
    MouseActivate := TMouseActivate.maNoActivateAndEat;
  end;

    // add into master
  if IsControlHot(imMasAdd, listMaster, FMasMouse, true)  or
    IsControlHot(lbMasAdd, listMaster, FMasMouse, true) then
  begin
    StartEdit(edMas, listMaster, imMasList.Left, lbMasText.Left + lbMasText.Width);
    MouseActivate := TMouseActivate.maNoActivateAndEat;
  end;
end;


// click on details (with consuming mouse event)
procedure TForm1.listDetailsMouseActivate(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y, HitTest: Integer;
  var MouseActivate: TMouseActivate);
begin
  // delete from details
  if IsControlHot(imDetDelete, listDetails, FDetMouse, false) then
  begin
    DeleteDetailsItem;
    MouseActivate := TMouseActivate.maNoActivateAndEat;
  end;

    // add into details
  if IsControlHot(lbDetAdd, listDetails, FDetMouse, true) then
  begin
    StartEdit(edDet, listDetails, lbDetText.Left, lbDetText.Left + lbDetText.Width);
    MouseActivate := TMouseActivate.maNoActivateAndEat;
  end;
end;


// mouse move in master
procedure TForm1.listMasterMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  FMasMouse.X := X;
  FMasMouse.Y := Y;
  UpdateMasterControls;
end;


// mouse move in details
procedure TForm1.listDetailsMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  FDetMouse.X := X;
  FDetMouse.Y := Y;
  UpdateDetailsControls;
end;


// update controls in master
procedure TForm1.UpdateMasterControls;
begin
  // cursor
  listMaster.Cursor := GetHotOrDefaultCursor(
    IsControlHot(imMasDelete, listMaster, FMasMouse, false) or
    IsControlHot(shMasColor, listMaster, FMasMouse, false) or
    IsControlHot(imMasAdd, listMaster, FMasMouse, true) or
    IsControlHot(lbMasAdd, listMaster, FMasMouse, true)
  );

  // add-label underlining
  var fs:TFontStyles := [];
  if IsControlHot(lbMasAdd, listMaster, FMasMouse, true) then
    fs := [TFontStyle.fsUnderline];

  if lbMasAdd.Font.Style <> fs then
  begin
    lbMasAdd.Font.Style := fs;
    listMaster.Invalidate;
  end;
end;


// update controls in details
procedure TForm1.UpdateDetailsControls;
begin
  // cursor
  listDetails.Cursor := GetHotOrDefaultCursor(
    IsControlHot(imDetDelete, listDetails, FDetMouse, false) or
    IsControlHot(lbDetAdd, listDetails, FDetMouse, true)
  );
end;


// cursor helper
function TForm1.GetHotOrDefaultCursor(IsHot: boolean): TCursor;
begin
  if IsHot then
    result := crHandPoint
  else
    result := crDefault;
end;


// determines if the mouse is over control
function TForm1.IsControlHot(AControl: TControl; AList: TControlList; AMouse: TPoint; LastRow: boolean): boolean;
var
  LRect: TRect;
begin
  if (AList.HotItemIndex = AList.ItemCount-1) <> LastRow then
    exit(false);

  LRect := AList.GetItemRect(AList.HotItemIndex);
  Inc(LRect.Left, AControl.Left);
  Inc(LRect.Top, AControl.Top);
  LRect.Right := LRect.Left + AControl.Width;
  LRect.Bottom := LRect.Top + AControl.Height;
  result := LRect.Contains(AMouse);
end;


// shows edit control
procedure TForm1.StartEdit(AEdit: TEdit; AList: TControlList; ALeft: integer; ARight: integer);
var
  LRect: TRect;
begin
  LRect := AList.GetItemRect(AList.HotItemIndex);
  AEdit.Left := LRect.Left + ALeft;
  AEdit.Top := LRect.Top;
  AEdit.Width := (ARight - ALeft);
  AEdit.Text := '';
  AEdit.Visible := true;
  AEdit.SetFocus;
end;


// ends editing when edit control loses focus
procedure TForm1.EditExit(Sender: TObject);
begin
  FinishEdit(Sender, false);
end;


// ends editing when ENTER or ESC is pressed in edit control
procedure TForm1.EditKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key = #13) or (Key = #27) then
  begin
    FinishEdit(Sender, Key = #13);
    Key := #0;
  end;
end;


// suppress mouse activity in edit control
procedure TForm1.EditMouseActivate(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y, HitTest: Integer;
  var MouseActivate: TMouseActivate);
begin
  MouseActivate := maNoActivate;
end;


// hides edit control and invokes its onclick handler
procedure TForm1.FinishEdit(Sender: TObject; Success: boolean);
begin
  with Sender as TEdit do
    if Visible then
    begin
      Visible := false;
      Text := trim(Text);
      if not Success then
        Text := '';

      if Assigned(OnClick) then
        OnClick(Sender);
    end;
end;


// on editing finished in master
procedure TForm1.edMasFinishEdit(Sender: TObject);
begin
  imMasAdd.Visible := true;
  lbMasAdd.Visible := true;
  if edMas.Text <> '' then
    AddMasterItem(edMas.Text);
  DoAfterItemSelectedMaster;
end;

// on editing finished in details
procedure TForm1.edDetClick(Sender: TObject);
begin
  lbDetAdd.Visible := true;
  if edDet.Text <> '' then
    AddDetailsItem(edDet.Text);
  DoAfterItemSelectedDetails;
end;


// export stub
procedure TForm1.imMasExportClick(Sender: TObject);
begin
  // todo: implement export
  var klist := FRepo.GetList(listMaster.ItemIndex);
  if klist <> nil then
    ShowMessage('����� ����� ������� ������ ' + klist.Name);
end;


// import stub
procedure TForm1.imMasImportClick(Sender: TObject);
begin
  // todo: implement import
  ShowMessage('����� ����� ������ ������ ������');
end;


// app exit
procedure TForm1.btCloseClick(Sender: TObject);
begin
  // todo: ask - are you really sure???
  Close;
end;


// loads default data
procedure TForm1.btDefaultClick(Sender: TObject);
begin
  // todo: it would be great to use resource file here
  FSaver.loadData(FRepo.KeywordsRepo,
    '[{"name":"���������","color":15710719,"keywords":["������","���������","�'+
    '���������","������������","��������","�������������"]},{"name":"���������'+
    '������� ������������","color":14448508,"keywords":["��������","����������'+
    '�� ��������������","����� ��������������","������������","���������������'+
    '� ��������������","����������������"]},{"name":"���������","color":435724'+
    '2,"keywords":["��� �����","��������","���������","�����������","������","'+
    '��������","������","������","���������","�����"]},{"name":"Cyber security'+
    '","color":8514685,"keywords":["Botnet","Brute forcing","DDOS","Hacker","M'+
    'alware","Mysql injection","Scammers","Spammer","Trojan","Virus","Worm"]},'+
    '{"name":"����������� � ��������. ���������","color":4308296,"keywords":["'+
    '�������������","������","��������","���������","������","���������� ����"'+
    ',"�����","�������������","�������"]}]'
  );
  UpdateMasterFromModel;
  cbMasAll.Checked := false;
end;


end.
