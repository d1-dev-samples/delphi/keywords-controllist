unit KeywordsSaver;

interface
uses
  System.JSON,
  System.IOUtils,
  System.Classes,
  System.SysUtils,
  Vcl.Dialogs,
  Vcl.Graphics,
  KeywordsRepo;

type

  TKeywordsSaver = class(TComponent)
  private
    FFilename: string;
    FEncoding: boolean;
  public
    constructor Create(AOwner: TComponent; const Filename: string; Encoding: boolean = false);

    function loadData(Repo: TKeywordsRepo; const Data: string): boolean;
    function load(Repo: TKeywordsRepo): boolean;

    function saveData(Repo: TKeywordsRepo; var Data: string): boolean;
    function save(Repo: TKeywordsRepo): boolean;
  end;

implementation

{ TKeywordsSaver }

constructor TKeywordsSaver.Create(AOwner: TComponent; const Filename: string; Encoding: boolean = false);
begin
  inherited Create(AOwner);
  FFilename := Filename;
  FEncoding := Encoding;
end;

function TKeywordsSaver.load(Repo: TKeywordsRepo): boolean;
begin
  result := false;
  try
    if TFile.Exists(FFilename) then
      loadData(Repo, TFile.ReadAllText(FFilename, TEncoding.UTF8));
    result := true;
  except
    ShowMessage('������ ������ ����� ' + FFilename);
  end;
end;


function TKeywordsSaver.loadData(Repo: TKeywordsRepo; const Data: string): boolean;
var
  LListArr: TJSONArray;
  LListObj: TJSONObject;
  LListName: string;
begin
  result := false;
  LListArr := nil;
  Repo.Clear;
  try
    try
      LListArr := TJSONArray.ParseJSONValue(Data) as TJSONArray;
      for var i:=0 to LListArr.Count-1 do
        begin
          LListObj := LListArr.Items[i].AsType<TJSONObject>;
          LListName := LListObj.Values['name'].Value;

          var k := Repo.Add(
            LListName,
            LListObj.Values['color'].AsType<integer>
          );

          var klist := Repo.Get(k);
          if klist <> nil then
            for var keywordObj in LListObj.Values['keywords'].AsType<TJSONArray> do
              klist.Add(keywordObj.Value);
        end;
      result := true;
    except
      ShowMessage('������ ������� ����� ' + FFilename);
    end;
  finally
    if LListArr <> nil then
      LListArr.Free;
  end;
end;


function TKeywordsSaver.save(Repo: TKeywordsRepo): boolean;
var
  LData: string;
begin
  result := false;
  try
    if saveData(Repo, LData) then
    begin
      TFile.WriteAllText(FFilename, LData, TEncoding.UTF8);
      result := true;
    end;
  except
    ShowMessage('������ ���������� � ���� ' + FFilename);
  end;
end;


function TKeywordsSaver.saveData(Repo: TKeywordsRepo; var Data: string): boolean;
var
  LListArr: TJSONArray;
  LKeywordArr: TJSONArray;
begin
  result := false;
  LListArr := TJSONArray.Create;
  try
    for var i := 0 to Repo.GetCount-1 do
    begin
      var klist := Repo.Get(i);
      LKeywordArr := TJSONArray.Create;
      for var j := 0 to klist.GetCount-1 do
        LKeywordArr.Add(klist.Get(j));

      LListArr.Add(
        TJSONObject.Create
          .AddPair('name', klist.Name)
          .AddPair('color', integer(klist.Color))
          .AddPair('keywords', LKeywordArr)
      );
    end;
    if FEncoding then
      Data := LListArr.ToJSON
    else
      Data := LListArr.ToString;
    result := true;
  finally
    LListArr.Free;
  end;
end;

end.
