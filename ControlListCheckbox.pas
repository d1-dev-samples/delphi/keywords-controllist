unit ControlListCheckbox;

interface
uses
  System.Classes,
  Vcl.Graphics,
  Vcl.ControlList,
  Vcl.Themes,
  Winapi.Windows;


type
  TControlListCheckbox = class(TControlListControl)
  private
    FChecked: boolean;
    FHot: boolean;
    procedure SetHot(AHot: boolean);
    procedure SetChecked(AChecked: boolean);
  protected
     procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    property Checked: boolean read FChecked write SetChecked;
    property Hot: boolean read FHot write SetHot;
  end;

implementation

{ TControlListCheckbox }

constructor TControlListCheckbox.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Width := GetSystemMetrics(SM_CXMENUCHECK);
  Height := GetSystemMetrics(SM_CYMENUCHECK);
end;

procedure TControlListCheckbox.Paint;
var
  LDetails: TThemedElementDetails;
  StyleService: TCustomStyleServices;
  LRect: TRect;
begin
  inherited;
  StyleService := StyleServices;

  if FChecked then
  begin
    if FHot then
      LDetails := StyleService.GetElementDetails(tbCheckBoxCheckedHot)
    else
      LDetails := StyleService.GetElementDetails(tbCheckBoxCheckedNormal);
  end
  else
  begin
    if FHot then
      LDetails := StyleService.GetElementDetails(tbCheckBoxUncheckedHot)
    else
      LDetails := StyleService.GetElementDetails(tbCheckBoxUncheckedNormal);
  end;

  LRect.Left := 0;
  LRect.Top := 0;
  LRect.Right := Width;
  LRect.Bottom := Height;

  StyleService.DrawElement(Canvas.Handle, LDetails, LRect);
end;

procedure TControlListCheckbox.SetChecked(AChecked: boolean);
begin
  FChecked := AChecked;
end;

procedure TControlListCheckbox.SetHot(AHot: boolean);
begin
  FHot := AHot;
end;

end.
