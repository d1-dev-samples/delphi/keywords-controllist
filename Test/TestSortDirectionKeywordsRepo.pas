unit TestSortDirectionKeywordsRepo;

interface

uses
  KeywordsRepo,
  SortDirKeywordsRepo,
  sysutils,
  DUnitX.TestFramework;

type
  [TestFixture]
  TTestSortDirectionKeywordsRepo = class
  private
    FRepo: TSortDirectionKeywordsRepo;
    function AddList: TKeywordList;
  public
    [Setup]
    procedure Setup;

    [TearDown]
    procedure TearDown;

    [Test] procedure TestGetList;
    [Test] procedure TestAddList;
    [Test] procedure TestRemoveList;

    [Test] procedure TestGetKeyword;
    [Test] procedure TestAddKeyword;
    [Test] procedure TestRemoveKeyword;

  end;

implementation


procedure TTestSortDirectionKeywordsRepo.Setup;
begin
  FRepo := TSortDirectionKeywordsRepo.Create(nil);
end;

procedure TTestSortDirectionKeywordsRepo.TearDown;
begin
  FRepo.Free;
end;


function TTestSortDirectionKeywordsRepo.AddList: TKeywordList;
begin
  result := FRepo.GetList(FRepo.AddList('L' + FRepo.GetListCount.ToString, FRepo.GetListCount));
end;


procedure TTestSortDirectionKeywordsRepo.TestGetList;
begin
  AddList;
  AddList;
  AddList;

  FRepo.ListsDir := sdAscending;
  Assert.AreEqual('L0', FRepo.GetList(0).Name, 'GetList 0 ascending');
  Assert.AreEqual('L1', FRepo.GetList(1).Name, 'GetList 1 ascending');
  Assert.AreEqual('L2', FRepo.GetList(2).Name, 'GetList 2 ascending');

  FRepo.ListsDir := sdDescending;
  Assert.AreEqual('L2', FRepo.GetList(0).Name, 'GetList 0 descending');
  Assert.AreEqual('L1', FRepo.GetList(1).Name, 'GetList 1 descending');
  Assert.AreEqual('L0', FRepo.GetList(2).Name, 'GetList 2 descending');

  Assert.IsNull(FRepo.GetList(-1), 'GetList -1');
  Assert.IsNull(FRepo.GetList(3), 'GetList 3');
end;


procedure TTestSortDirectionKeywordsRepo.TestAddList;
begin
  FRepo.ListsDir := sdAscending;
  Assert.AreEqual(0, FRepo.AddList('L1', 1), 'AddList 1');
  Assert.AreEqual(1, FRepo.AddList('L2', 2), 'AddList 2');
  Assert.AreEqual(2, FRepo.AddList('L3', 3), 'AddList 3');
  Assert.AreEqual(0, FRepo.AddList('L0', 0), 'AddList 0');
  Assert.AreEqual(4, FRepo.AddList('L4', 4), 'AddList 4');

  FRepo.ListsDir := sdDescending;
  Assert.AreEqual(0, FRepo.AddList('L6', 6), 'AddList 5');
  Assert.AreEqual(0, FRepo.AddList('L7', 7), 'AddList 7');
  Assert.AreEqual(2, FRepo.AddList('L5', 5), 'AddList 5');

  Assert.AreEqual(-1, FRepo.AddList('L5', 5), 'Dublicate AddList 5');
  Assert.AreEqual(8, FRepo.GetListCount, 'GetListCount');
end;


procedure TTestSortDirectionKeywordsRepo.TestRemoveList;
begin
  AddList;
  AddList;
  AddList;
  AddList;

  FRepo.ListsDir := sdAscending;
  Assert.AreEqual(true, FRepo.RemoveList(1), 'RemoveList 1');
  Assert.AreEqual('L0', FRepo.GetList(0).Name, 'GetList 0 after RemoveList 1');
  Assert.AreEqual('L2', FRepo.GetList(1).Name, 'GetList 1 after RemoveList 1');
  Assert.AreEqual('L3', FRepo.GetList(2).Name, 'GetList 2 after RemoveList 1');

  FRepo.ListsDir := sdDescending;
  Assert.AreEqual(true, FRepo.RemoveList(0), 'RemoveList 0');
  Assert.AreEqual('L2', FRepo.GetList(0).Name, 'GetList 0 after RemoveList 0');
  Assert.AreEqual('L0', FRepo.GetList(1).Name, 'GetList 1 after RemoveList 0');

  Assert.AreEqual(false, FRepo.RemoveList(-1), 'RemoveList -1');
  Assert.AreEqual(false, FRepo.RemoveList(4), 'RemoveList 4');
end;



procedure TTestSortDirectionKeywordsRepo.TestGetKeyword;
begin
  AddList;
  FRepo.AddKeyword(0, 'K0');
  FRepo.AddKeyword(0, 'K1');
  FRepo.AddKeyword(0, 'K2');

  FRepo.KeywordsDir := sdAscending;
  Assert.AreEqual('K0', FRepo.GetKeyword(0, 0), 'GetKeyword 0 ascending');
  Assert.AreEqual('K1', FRepo.GetKeyword(0, 1), 'GetKeyword 1 ascending');
  Assert.AreEqual('K2', FRepo.GetKeyword(0, 2), 'GetKeyword 2 ascending');

  FRepo.KeywordsDir := sdDescending;
  Assert.AreEqual('K2', FRepo.GetKeyword(0, 0), 'GetKeyword 0 descending');
  Assert.AreEqual('K1', FRepo.GetKeyword(0, 1), 'GetKeyword 1 descending');
  Assert.AreEqual('K0', FRepo.GetKeyword(0, 2), 'GetKeyword 2 descending');

  Assert.AreEqual('', FRepo.GetKeyword(0, -1), 'GetKeyword -1');
  Assert.AreEqual('', FRepo.GetKeyword(0, 3), 'GetKeyword 3');
  Assert.AreEqual('', FRepo.GetKeyword(1, 0), 'GetKeyword no list');
end;


procedure TTestSortDirectionKeywordsRepo.TestAddKeyword;
begin
  AddList;

  FRepo.KeywordsDir := sdAscending;
  Assert.AreEqual(0, FRepo.AddKeyword(0, 'K1'), 'AddKeyword 1');
  Assert.AreEqual(1, FRepo.AddKeyword(0, 'K2'), 'AddKeyword 2');
  Assert.AreEqual(2, FRepo.AddKeyword(0, 'K3'), 'AddKeyword 3');
  Assert.AreEqual(0, FRepo.AddKeyword(0, 'K0'), 'AddKeyword 0');
  Assert.AreEqual(4, FRepo.AddKeyword(0, 'K4'), 'AddKeyword 4');

  FRepo.KeywordsDir := sdDescending;
  Assert.AreEqual(0, FRepo.AddKeyword(0, 'K6'), 'AddKeyword 5');
  Assert.AreEqual(0, FRepo.AddKeyword(0, 'K7'), 'AddKeyword 7');
  Assert.AreEqual(2, FRepo.AddKeyword(0, 'K5'), 'AddKeyword 5');

  Assert.AreEqual(-1, FRepo.AddKeyword(0, 'K5'), 'Dublicate AddKeyword 5');
  Assert.AreEqual(8, FRepo.GetKeywordCount(0), 'GetListCount');
end;

procedure TTestSortDirectionKeywordsRepo.TestRemoveKeyword;
begin
  AddList;
  FRepo.AddKeyword(0, 'K0');
  FRepo.AddKeyword(0, 'K1');
  FRepo.AddKeyword(0, 'K2');
  FRepo.AddKeyword(0, 'K3');

  FRepo.KeywordsDir := sdAscending;
  Assert.AreEqual(true, FRepo.RemoveKeyword(0, 1), 'RemoveKeyword 1');
  Assert.AreEqual('K0', FRepo.GetKeyword(0, 0), 'GetKeyword 0 after RemoveKeyword 1');
  Assert.AreEqual('K2', FRepo.GetKeyword(0, 1), 'GetKeyword 1 after RemoveKeyword 1');
  Assert.AreEqual('K3', FRepo.GetKeyword(0, 2), 'GetKeyword 2 after RemoveKeyword 1');

  FRepo.KeywordsDir := sdDescending;
  Assert.AreEqual(true, FRepo.RemoveKeyword(0, 0), 'RemoveKeyword 0');
  Assert.AreEqual('K2', FRepo.GetKeyword(0, 0), 'GetKeyword 0 after RemoveKeyword 0');
  Assert.AreEqual('K0', FRepo.GetKeyword(0, 1), 'GetKeyword 1 after RemoveKeyword 0');

  Assert.AreEqual(false, FRepo.RemoveKeyword(0, -1), 'RemoveKeyword -1');
  Assert.AreEqual(false, FRepo.RemoveKeyword(0, 4), 'RemoveKeyword 4');
end;



initialization
  TDUnitX.RegisterTestFixture(TTestSortDirectionKeywordsRepo);

end.
