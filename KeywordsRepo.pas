unit KeywordsRepo;

interface
uses
  Vcl.Graphics,
  System.Classes,
  Generics.Collections;

type

  TKeywordList = class
  private
    FName: string;
    FKeywords: TStringList;
    FColor: TColor;
    FChecked: boolean;
  public
    constructor Create(const Name: string; Color: TColor);
    destructor Destroy; override;
    function Add(const Keyword: string): integer; virtual;
    function Remove(Index: integer): boolean; virtual;
    function Get(Index: integer): string; virtual;
    function GetCount: integer;

    property Name: string read FName;
    property Color: TColor read FColor write FColor;
    property Checked: boolean read FChecked write FChecked;
  end;


  TKeywordsRepo = class(TComponent)
  private
    FLists: TStringList;
  public
    constructor Create(AOwner: TComponent);
    destructor Destroy; override;
    function Add(const AListName: string; AColor: TColor): integer; virtual;
    function Remove(Index: integer): boolean; virtual;
    function Get(Index: integer): TKeywordList; virtual;
    function GetCount: integer;
    procedure Clear;
  end;

implementation

{ TKeywordList }

constructor TKeywordList.Create(const Name: string; Color: TColor);
begin
  FName := Name;
  FColor := Color;
  FKeywords := TStringList.Create;
  FKeywords.Sorted := true;
  FKeywords.Duplicates := dupError;
end;


destructor TKeywordList.Destroy;
begin
  FKeywords.Free;
end;


function TKeywordList.Add(const Keyword: string): integer;
begin
  try
    result := FKeywords.Add(Keyword);
  except
    result := -1;
  end;
end;


function TKeywordList.Get(Index: integer): string;
begin
  if (Index < 0) or (Index >= FKeywords.Count) then
    exit('');
  result := FKeywords[Index];
end;


function TKeywordList.GetCount: integer;
begin
  result := FKeywords.Count;
end;


function TKeywordList.Remove(Index: integer): boolean;
begin
  if (Index < 0) or (Index >= FKeywords.Count) then
    exit(false);
  FKeywords.Delete(Index);
  result := true;
end;


{ TKeywordsRepo }

constructor TKeywordsRepo.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FLists := TStringList.Create;
  FLists.Sorted := true;
  FLists.Duplicates := dupError;
end;


destructor TKeywordsRepo.Destroy;
begin
  Clear;
  FLists.Free;
end;


procedure TKeywordsRepo.Clear;
begin
  for var i := 0 to FLists.Count-1 do
    FLists.Objects[i].Free;
  FLists.Clear;
end;


function TKeywordsRepo.Add(const AListName: string; AColor: TColor): integer;
begin
  try
    result := FLists.Add(AListName);
    FLists.Objects[result] := TKeywordList.Create(AListName, AColor);
  except
    result := -1;
  end;
end;


function TKeywordsRepo.Get(Index: integer): TKeywordList;
begin
  if (Index < 0) or (Index >= FLists.Count) then
    exit(nil);
  result := TKeywordList(FLists.Objects[Index]);
end;


function TKeywordsRepo.GetCount: integer;
begin
  result := FLists.Count;
end;


function TKeywordsRepo.Remove(Index: integer): boolean;
begin
  if (Index < 0) or (Index >= FLists.Count) then
    exit(false);
  FLists.Objects[Index].Free;
  FLists.Delete(Index);
  result := true;
end;


end.
